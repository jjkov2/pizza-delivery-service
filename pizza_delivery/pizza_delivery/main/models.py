from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.utils.translation import gettext_lazy as _
from django.urls import reverse
from django.utils import timezone

from .manager import CustomUserManager

class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_("email address"), unique=True)
    username = models.CharField(max_length=150)
    is_staff = models.BooleanField(_("Работник?") ,default=False)
    is_active = models.BooleanField(_("Активный?"), default=True)
    date_joined = models.DateTimeField(_("Дата регистрации") ,default=timezone.now)
    photo = models.ImageField(_("Аватар"), upload_to="avatars/%Y%M%d")

    USERNAME_FIELD = "email"

    REQUIRED_FIELDS = ["username", "password"]

    objects = CustomUserManager()

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

        def __str__(self):
            return self.email
