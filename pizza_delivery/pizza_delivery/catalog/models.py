from django.db import models
from django.urls import reverse
class Pizza(models.Model):
    title = models.CharField(max_length=150, verbose_name="Название")
    price = models.IntegerField(verbose_name="Цена пиццы")
    composition = models.TextField(verbose_name="Состав пиццы")
    photo = models.ImageField(upload_to="pizzas", verbose_name="Фотография пиццы")
    class Meta:
        verbose_name = "Пицца"
        verbose_name_plural = "Пиццы"

    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        """Функция для перехода к пицце по id"""
        return reverse("view_pizza", kwargs={"pizza_id": self.pk})
class Topping(models.Model):
    title = models.CharField(max_length=150, verbose_name="Название")
    price = models.IntegerField(verbose_name="Стоимость топпинга")
    photo = models.ImageField(upload_to="toppings", verbose_name="Фотография топпинга")

    class Meta:
        verbose_name = "Топпинг"
        verbose_name_plural = "Топпинги"