# Generated by Django 4.1.4 on 2022-12-21 12:10

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Pizza',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=150, verbose_name='Название')),
                ('price', models.IntegerField(verbose_name='Цена пиццы')),
                ('composition', models.TextField(verbose_name='Состав пиццы')),
                ('photo', models.ImageField(upload_to='media/pizzas/%Y%m%d', verbose_name='Фотография пиццы')),
            ],
            options={
                'verbose_name': 'Пицца',
                'verbose_name_plural': 'Пиццы',
            },
        ),
        migrations.CreateModel(
            name='Topping',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=150, verbose_name='Название')),
                ('price', models.IntegerField(verbose_name='Стоимость топпинга')),
                ('photo', models.ImageField(upload_to='media/toppings/%Y%m%d', verbose_name='Фотография топпинга')),
            ],
            options={
                'verbose_name': 'Топпинг',
                'verbose_name_plural': 'Топпинги',
            },
        ),
    ]
