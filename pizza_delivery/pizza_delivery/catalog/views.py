from django.shortcuts import render

from .models import Pizza

def index(request):
    """Функция для отображения каталога пицц"""
    pizzas = Pizza.objects.all()
    context = {"pizzas": pizzas}
    return render(request, "catalog/index.html", context)

def view_pizza(request, pizza_id):
    """Функция для отображения страницы пиццы"""
    pizza = Pizza.objects.get(id=pizza_id)
    context = {"pizza": pizza}
    return render(request, "catalog/view_pizza.html", context)