from django.urls import path

from .views import *

urlpatterns = [
    path("", index, name="main"),
    path("pizza/<int:pizza_id>", view_pizza, name="view_pizza"),
]
